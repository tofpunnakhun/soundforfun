package com.example.soundforfun;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.Button;

import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {
	
	private Button startb;
	MediaPlayer mpbg;
	String c ="";
	int highS;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        File infile = getBaseContext().getFileStreamPath("province.dat");
		if (infile.exists()) {
			try {
				BufferedReader reader = new BufferedReader( new FileReader (infile));
				
				c = reader.readLine().trim();
				highS =Integer.parseInt(c);
				reader.close();
			} catch (Exception e) {
				//Do nothing
				 
			}
		}
		
		
		TextView hscore = (TextView)findViewById(R.id.tv1);
		hscore.setText(c);
		final Button btn1 = (Button) findViewById(R.id.Button3);
		final AlertDialog.Builder adb = new AlertDialog.Builder(this);
		btn1.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
			
			
			
			
			adb.setTitle("How to play");
			adb.setMessage(
					" 1. Turn on the audio, check the sound, make sure that you can here the sound.\n" +
					"2. When you ready, click on the start icon and the game will start immediately. Careful !\n" +
					"3. Listen carefully to the pokemon's sound, it will start suddenly when you clicked on start.\n" +
					"4. If you click the wrong pokemon, it will random the new pokemon's sound.\n" +
					"5. You have only 30 seconds to challenge.\n" +
					"6. Do your best, we hope you to enjoy the game. " );
			adb.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener(){
				
				public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				}
				});
				adb.create();
				adb.show();
				}  
				
				});
		
		
		
        
        startb=(Button)findViewById(R.id.bstart);
        startb.setOnClickListener(this);
        
         mpbg = MediaPlayer.create(MainActivity.this, R.raw.opening);
         mpbg.start();
         mpbg.setOnCompletionListener(new OnCompletionListener() {
     	    public void onCompletion(MediaPlayer mp) {
     	        mp.release();
     	    }
     	});
         
        
    }
    
    public void onStop() {
        super.onStop();
        try {
            if(mpbg.isPlaying()) {
                mpbg.stop();
                mpbg.release();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }
 

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
        
        
            }

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		onStop();
		
		Intent goGame = new Intent(getApplicationContext(),game.class); 
		startActivity(goGame);
		
		
		
		
	}
    
}
